package sbu.cs.parser.json;

public class JsonString implements JsonType{

    private String value;

    public void setValue(String value) {
        this.value = value;
    }

    public boolean is (String value) {

        return true;
    }

    public void process (String value) {

        setValue(value);
    }
}
