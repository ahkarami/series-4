package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.List;

public class Json implements JsonInterface {

    private List<String> keys;
    private List<String> stringValues;
    private List<JsonType> exactValues;

    public Json (){
        keys = new ArrayList<>();
        stringValues = new ArrayList<>();
    }

    public void addKey (String key){
        this.keys.add(key);
    }

    public void addStringValue (String value){
        this.stringValues.add(value);
    }

    public void addExactValue (JsonType value){ this.exactValues.add(value); }

    public List<String> getKeys() {
        return keys;
    }

    public List<String> getStringValues() {
        return stringValues;
    }

    public List<JsonType> getExactValues() {
        return exactValues;
    }

    @Override
    public String getStringValue(String key) {

        int index = 0;
         while (true)
         {
             if (index > this.getKeys().size() - 1)
             {
                 return " No Match";
             }

             if (this.getKeys().get(index).compareTo(key) == 0)
             {
                 return this.getStringValues().get(index);
             }

             index++;
         }


    }
}
