package sbu.cs.parser.json;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonNumber implements JsonType{

    private int sahih;
    private int ashar;

    public void setSahih(int sahih) {

        this.sahih = sahih;
    }

    public void setAshar(int ashar) {

        this.ashar = ashar;
    }

    public boolean is (String value) {

        boolean flag = false;

        Pattern pattern = Pattern.compile("[0-9|.]*");
        Matcher matcher = pattern.matcher(value);

        if (matcher.matches())
        {
            flag = true;
        }

        return flag;
    }

    public void process (String value) {

        setSahih(Integer.parseInt(value.split(".")[0]));
        setAshar(Integer.parseInt(value.split(".")[1]));
    }

    public int getAshar() {
        return ashar;
    }

    public int getSahih() {
        return sahih;
    }
}
