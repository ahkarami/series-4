package sbu.cs.parser.json;

public class JsonNull implements JsonType{

    public String getValue() {

        return null;
    }

    public boolean is (String value) {

        if (value.compareTo("null") == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    @Override
    public void process(String value) { }
}
