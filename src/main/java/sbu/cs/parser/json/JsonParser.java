package sbu.cs.parser.json;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonParser {

    /*
    * this function will get a String and returns a Json object
     */
    public static Json parse(String data) {

        String newData = data;
        newData = newData.replaceAll("[{}\\n\\t\\\"]", "");

        Json json = new Json();


        setArrays(newData, json);

        newData = removeArrays(newData);

        setOtherKeys(newData, json);
        setOtherValues(newData, json);


        return json;
    }

    //--------------*********-----------------------------------------------------------------
    public static void setArrays(String data, Json json)
    {
        String newData = data;

//        newData = newData.replaceAll("[{}\\n\\t\\\"]", "");
//        System.out.println(newData);// print


        Pattern arrayPattern = Pattern.compile("[\\w|\\d]+:\\s*\\[[\\d|\\w|,|\\s]*\\],");
        Matcher arrayMatcher = arrayPattern.matcher(newData);

        List<String> arrayData = new ArrayList<>();

        while (arrayMatcher.find()) {
            StringBuffer tmp = new StringBuffer(arrayMatcher.group());
            arrayData.add(tmp.deleteCharAt(tmp.length() - 1).toString());
        }

//        System.out.println(arrayData);


        // adding keys & values
        for (int i = 0; i < arrayData.size(); i++) {
            json.addKey(arrayData.get(i).split(":")[0]);
            json.addStringValue(arrayData.get(i).split(":")[1].trim());

            JsonArray jsonArray = new JsonArray();
            jsonArray.process(arrayData.get(i).split(":")[1].trim());
//            json.addExactValue(jsonArray);

        }


    }

    public static String removeArrays (String data) {

        return data.replaceAll("[\\w|\\d]+:\\s*\\[[\\d|\\w|,|\\s]*\\],", "");

    }
    //--------------------------************-----------------------------------------------

    public static void setOtherKeys (String data, Json json) {

        String newData = data;

        newData = newData.replaceAll("[{}\\n\\t\\\"]","");
//        System.out.println(newData);

        Pattern pattern = Pattern.compile("[a-z|A-Z|0-9]*:");
        Matcher matcher = pattern.matcher(newData);

        while (matcher.find())
        {
            json.addKey(matcher.group().replaceAll(":", ""));
        }

        return;
    }

    public static void setOtherValues (String data, Json json) {

        String newData = data;

        newData = newData.replaceAll("[{}\\n\\t\\\"]","");
//        System.out.println(newData);

        Pattern pattern = Pattern.compile(":[a-z|A-Z|0-9|\\s]*");
        Matcher matcher = pattern.matcher(newData);

        while (matcher.find())
        {
            json.addStringValue(matcher.group().replaceAll(":", "").trim());
//            setOtherExactValues(matcher.group().replaceAll(":", "").trim(), json);
        }

        return;
    }

    public static void setOtherExactValues (String value, Json json) {

        JsonBoolean jsonBoolean = new JsonBoolean();
        JsonNull jsonNull = new JsonNull();
        JsonNumber jsonNumber = new JsonNumber();
        JsonString jsonString = new JsonString();

        if (jsonBoolean.is(value))
        {
            jsonBoolean.process(value);
            json.addExactValue(jsonBoolean);
        }
        else if (jsonNull.is(value))
        {
            jsonNull.process(value);
            json.addExactValue(jsonNull);
        }
        else if (jsonNumber.is(value))
        {
            jsonNumber.process(value);
            json.addExactValue(jsonNumber);
        }
        else
        {
            jsonString.process(value);
            json.addExactValue(jsonString);
        }
    }

    /*
    * this function is the opposite of above function. implementing this has no score and
    * is only for those who want to practice more.
     */
    public static String toJsonString(Json data) {
        return null;
    }
}
