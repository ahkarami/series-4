package sbu.cs.parser.json;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JsonArray implements JsonType {

    private String[] array;

    public void setAttributes (int index, String element) {

        this.array[index] = element;
    }

    public boolean is (String value) {

        boolean flag = false;

        Pattern pattern = Pattern.compile("\\[[\\w|\\d|\\s|,]*\\]");
        Matcher matcher = pattern.matcher(value);

        if (matcher.matches())
        {
            flag = true;
        }

        return flag;
    }

    public void process (String value) {

        this.array = value.replaceAll("\\[", "").replaceAll("\\]", "")
                .replaceAll(" ", "").split(",");

    }

    public String[] getArray() {
        return array;
    }



}
