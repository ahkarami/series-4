package sbu.cs.parser.json;

public class JsonBoolean implements JsonType{

    private boolean value;

    public void setValue (boolean value) {

        this.value = value;
    }

    public boolean is (String value) {

        boolean flag = false;

        if (value.compareTo("true") == 0 || value.compareTo("false") == 0)
        {
            flag = true;
        }

        return flag;
    }

    public void process (String value) {


        if (value.compareTo("true") == 0)
        {
            setValue(true);
        }
        else if (value.compareTo("false") == 0)
        {
            setValue(false);
        }
    }
}
