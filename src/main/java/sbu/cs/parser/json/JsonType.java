package sbu.cs.parser.json;

public interface JsonType {

    boolean is(String value);
    void process(String value);
}
